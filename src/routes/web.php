<?php

use Byambasuren\Form\Controllers\FormController;
use Illuminate\Support\Facades\Route;


Route::get('/form', FormController::class);
