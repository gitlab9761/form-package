<?php
namespace Byambasuren\Form\Controllers;

use Illuminate\Http\Request;
use Byambasuren\Form\Form;

class FormController
{
    public function __invoke(Request $request): string
    {
        $quote = "Byambasuren";

        return view('form::form.index', compact('quote'));
    }
}
